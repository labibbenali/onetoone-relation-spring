package com.oneToOne.Controller;

import com.oneToOne.model.Instructor;
import com.oneToOne.service.InstructorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/onetoone")
public class InstructorController {
    private final InstructorService instructorService;
    private Instructor instructor;

    @Autowired
    public InstructorController(InstructorService instructorService) {
        this.instructorService = instructorService;
    }

    @GetMapping("/instructors")
    public List<Instructor> getnstructors(){
        return  this.instructorService.getAllInstructor();
    }

    @PostMapping("/instructors")
    public Instructor createInstructor(@RequestBody Instructor instructor){
        return this.instructorService.createInstructor(instructor);
    }
    @DeleteMapping("/instructors/{id}")
    public String deleteInstructor(@PathVariable("id") Long instructorId){
        try {
            Instructor instructor = this.instructorService.getInstructorById(instructorId);
            if (instructor != null) {
                this.instructorService.deleteInstructor(instructorId);
                return "Done !";
            }

            return "not exist";
        }catch(Exception e ){ return "Id not exist  : "+e ;}
    }
}
