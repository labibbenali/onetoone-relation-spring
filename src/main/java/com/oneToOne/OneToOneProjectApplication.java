package com.oneToOne;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OneToOneProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(OneToOneProjectApplication.class, args);
	}

}
