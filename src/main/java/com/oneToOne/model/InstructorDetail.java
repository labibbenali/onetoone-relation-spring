package com.oneToOne.model;

import javax.persistence.*;

@Entity
@Table(name="instructor_detail")
public class  InstructorDetail {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	@Column(name="youtube_channel")
	private String youtubeChannel;
	@Column(name="hobby")
	private String hobby;
	///*
	//we add this lines to make the relationship between instructor entity and instructorDetail entity bidirectional
	// so 	@OneToOne(mappedBy = "instructorDetail") and 	private Instructor instructor not necessary we must add
	//when we want a bidirectional relationship
	//the "instructorDetail" in mappedBy referee to instructorDetail in instructor entity

	@OneToOne(mappedBy = "instructorDetail",cascade=CascadeType.ALL)
	private Instructor instructor;
	//*/
	public InstructorDetail(String youtubeChannel, String hobby,Instructor instructor) {
		this.youtubeChannel = youtubeChannel;
		this.hobby = hobby;
		this.instructor=instructor;
	}
	public InstructorDetail() {
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getYoutubeChannel() {
		return youtubeChannel;
	}
	public void setYoutubeChannel(String youtubeChannel) {
		this.youtubeChannel = youtubeChannel;
	}
	public String getHobby() {
		return hobby;
	}
	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	 // /*
	//we added setter and getter for instructor when we want to relationship be bidirectional
	public Instructor getInstructor() {
		return instructor;
	}

	public void setInstructor(Instructor instructor) {
		this.instructor = instructor;
	}
	//*/
	@Override
	public String toString() {
		return "InstructoDetail [id=" + id + ", youtubeChannel=" + youtubeChannel + ", hobby=" + hobby + "]";
	}
	
}
