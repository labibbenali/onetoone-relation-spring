package com.oneToOne.service;

import com.oneToOne.model.Instructor;
import com.oneToOne.repository.InstructorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InstructorService {

    private final InstructorRepository instructorRepository;
    @Autowired
    public InstructorService(InstructorRepository instructorRepository) {
        this.instructorRepository = instructorRepository;
    }

    public List<Instructor> getAllInstructor(){
        return  this.instructorRepository.findAll();
    }
    public Instructor getInstructorById(Long id){
        return this.instructorRepository.getById(id);
    }

    public Instructor createInstructor(Instructor instructor){
        return this.instructorRepository.save(instructor);
    }

    public void deleteInstructor(Long instructorId){
        this.instructorRepository.deleteById(instructorId);
    }

}
